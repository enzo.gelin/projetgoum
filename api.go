package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"time"
)

func helloHandler(w http.ResponseWriter, req *http.Request) {
	switch req.Method {
	case http.MethodGet:
		fmt.Fprintf(w, "Hello world")
	case http.MethodPost:
		if err := req.ParseForm(); err != nil {
			fmt.Println("Something went bad")
			fmt.Fprintln(w, "Something went bad")
			return
		}
		for key, value := range req.PostForm {
			fmt.Println(key, "=>", value)
		}
		fmt.Fprintf(w, "Information received: %v\n", req.PostForm)
	}
}

// Heure affichage
func timeHandler(w http.ResponseWriter, req *http.Request) {
	t := time.Now()
	formatted := fmt.Sprintf("%02d:%02d", t.Hour(), t.Minute())
	switch req.Method {
	case http.MethodGet:
		fmt.Fprintf(w, formatted)
	case http.MethodPost:
		if err := req.ParseForm(); err != nil {
			fmt.Println("Something went bad")
			fmt.Fprintln(w, "Something went bad")
			return
		}
		for key, value := range req.PostForm {
			fmt.Println(key, "=>", value)
		}
		fmt.Fprintf(w, "Information received: %v\n", req.PostForm)
	}
}

func addHandler(w http.ResponseWriter, req *http.Request) {

	switch req.Method {
	case http.MethodGet:
		fmt.Println("addHandler GET")
	case http.MethodPost:
		fmt.Println("addHandler POST")
		if err := req.ParseForm(); err != nil {
			fmt.Println("Something went bad")
			fmt.Fprintln(w, "Something went bad")
			return
		}
		req.ParseForm()
		w.WriteHeader(200)
		contents, _ := ioutil.ReadFile("data.txt") // texte dans le fichier avant modif
		str3 := req.FormValue("author")
		str4 := req.FormValue("entry")
		result := "[" + str3 + "]" + ":" + "[" + str4 + "]\r" // nouveau texte à écrire dans le fichier [author]:[entry]
		resultBytes := []byte(result)
		println(string(contents))
		concatByte := append(contents, resultBytes...)
		ioutil.WriteFile("data.txt", concatByte, 0644)

	}
}

func entrieHandler(w http.ResponseWriter, req *http.Request) {
	switch req.Method {
	case http.MethodGet:
		file, err := os.Open("data.txt")

		if err != nil {
			log.Fatalf("failed to open")

		}
		scanner := bufio.NewScanner(file)
		scanner.Split(bufio.ScanLines)
		var text []string

		for scanner.Scan() {
			text = append(text, scanner.Text())
		}

		file.Close()

		for _, each_ln := range text {
			fmt.Fprintf(w, each_ln)
		}

	case http.MethodPost:
		if err := req.ParseForm(); err != nil {
			fmt.Println("Something went bad")
			fmt.Fprintln(w, "Something went bad")
			return
		}
		for key, value := range req.PostForm {
			fmt.Println(key, "=>", value)
		}
		fmt.Fprintf(w, "Information received: %v\n", req.PostForm)
	}
}

func main() {
	http.HandleFunc("/", timeHandler)
	http.HandleFunc("/hello", helloHandler)
	http.HandleFunc("/add", addHandler)
	http.HandleFunc("/entries", entrieHandler)
	http.ListenAndServe(":4567", nil)
}
